use std::path::Path;
use std::process::{Command, Output};

use crate::audio_format::AudioFormat;
use crate::image_format::ImageFormat;

#[cfg(not(target_os = "windows"))]
pub const FFMPEG_BINARY: &str = "ffmpeg";

#[cfg(target_os = "windows")]
pub const FFMPEG_BINARY: &str = "ffmpeg.exe";

pub enum MediaFormat<'a> {
    Audio(&'a AudioFormat),
    Image(&'a ImageFormat)
}

pub fn transcode(input_file: &Path, output_file: &Path, target_format: MediaFormat) -> Result<(), String> {
    let mut command = Command::new(FFMPEG_BINARY);
    
    command.arg("-y");
    command.arg("-i").arg(input_file);
    
    match target_format {
        MediaFormat::Audio(format) => match format {
            AudioFormat::Mp3Cbr128 => {
                command.arg("-codec:a").arg("libmp3lame");
                command.arg("-b:a").arg("128");
            }
            AudioFormat::Mp3Cbr320 => {
                command.arg("-codec:a").arg("libmp3lame");
                command.arg("-b:a").arg("320");
            }
            AudioFormat::Mp3VbrV0 => {
                command.arg("-codec:a").arg("libmp3lame");
                command.arg("-qscale:a").arg("0");
            }
            _ => ()
        }
        _ => ()
    }
    
    command.arg(output_file);

    match command.output() {
        Ok(output) => {
            if output.status.success() {
                Ok(())
            } else {
                let ffmpeg_output = transcode_debug_output(output);
                Err(format!("The ffmpeg child process returned an error exit code.\n\n{}", ffmpeg_output))
            }
        }
        Err(_) => Err("The ffmpeg child process could not be executed.".to_string())
    }
}

fn transcode_debug_output(output: Output) -> String {
    let stderr = String::from_utf8(output.stderr).unwrap();
    let stdout = String::from_utf8(output.stdout).unwrap();

    format!("stderr: {}\n\nstdout: {}", stderr, stdout)
}